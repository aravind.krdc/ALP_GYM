<?php
session_start();
if (!isset($_SESSION["user"])) {
    $_SESSION["msg"] = "Unauthorised Access";
    header("Location: logoutAction.php");
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Fee Details</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="homestyle.css">

        <style>
            #menu button
            {
                height: 60px;
            }
        </style>
    </head>
    <?php
    $message = "";
    if (isset($_SESSION["msg"]))
        $message = $_SESSION["msg"];


    include './functions/datepicker_jquery.php';
    ?>
    <body>
        <div class="container-fluid">
            <div class="row" id="head">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6" id="logo">
                    <center><img src="images for html/ALLEPPEY GYM white.png">
                        <h2><b>ALLEPPY GYM</b></h2>
                        <p>Welcome admin</p></center>
                </div>
                <div class="col-md-3" id="logohome">
                    <p><a href="logoutAction.php">logout</a></p>
                </div>
            </div>            <
            <div class="contentwrapper">
                <div class="row" > 

                    <nav>
                        <span class="menu-btn">MENU</span>
                        <ul class="menu">
                            <li>
                                <a href="dashboard.php">
                                    <button type="button" class="w3-btn">Dashoard</button>
                                </a>
                            </li>
                            <li>
                                <a href="memberdetails.php">
                                    <button type="button" class="w3-btn">Member details</button>
                                </a>
                            </li>
                            <li>
                                <a href="feeDetails.php"> 
                                    <button type="button" class="w3-btn">Fees details</button>
                                </a>
                            </li>
                            <li>
                                <a href="addmember.php">
                                    <button type="button" class="w3-btn">Add new member</button>
                                </a>
                            </li>

                            <li>
                                <a href="monthlyAttendance.php">
                                    <button type="button" class="w3-btn">Monthly Attendance </button>
                                </a>
                            </li>
                            <li>
                                <a href="addPayment.php">
                                    <button type="button" class="w3-btn">Add Payment</button>
                                </a>
                            </li>
                            <li><a href="changePassword.php">
                                    <button type="button" class="w3-btn">Change Password</button>
                                </a>
                            </li>
                        </ul>

                    </nav>                 


                   <!-- <center> 
                        <div  class="col-lg-2">
                            <a href="dashboard.php">
                                <button type="button" class="w3-btn">Dashoard</button>
                            </a>
                        </div>
                        <div  class="col-lg-2">
                            <a href="memberdetails.php">
                                <button type="button" class="w3-btn">Member details</button>
                            </a>
                        </div>
                        <div  class="col-lg-2">
                            <a href="" >
                                <button type="button" class="w3-btn">Fees details</button>
                            </a>
                        </div>
                        <div  class="col-lg-2">
                            <a href="addmember.php">
                                <button type="button" class="w3-btn">Add new member</button>
                            </a>
                        </div>
                        
                        <div  class="col-lg-2">
                            <a href="monthlyAttendance.php">
                                <button type="button" class="w3-btn">Monthly Attendance </button>
                            </a>
                        </div>
                        
                        <div  class="col-lg-2">
                            <a href="addPayment.php">
                                <button type="button" class="w3-btn">Add Payment</button>
                            </a>
                        </div>
                    </center>-->
                </div>
            </div>
            <div class="row">

            </div>
            <div class="row">
                <div class="col-lg-1"> </div>



                <div class="col-lg-10">
                    <div class="table-responsive">
                        <table class="table table-bordered table-prop" >
                            <caption style="color: #fff;"><b>FEE DETAILS </b></caption>
                            <tr>
                                <th>Name</th>
                                <th>Admission</th>
                                <th>Advance</th>
                                <?php
                                $month = date("m");
                                $year = date("Y");
                                $count = 0;
                                while ($count < 5) {
                                    switch ($month) {
                                        case 1: $text = "JAN";
                                            break;
                                        case 2: $text = "FEB";
                                            break;
                                        case 3: $text = "MAR";
                                            break;
                                        case 4: $text = "APR";
                                            break;
                                        case 5: $text = "MAY";
                                            break;
                                        case 6: $text = "JUN";
                                            break;
                                        case 7: $text = "JUL";
                                            break;
                                        case 8: $text = "AUG";
                                            break;
                                        case 9: $text = "SEP";
                                            break;
                                        case 10: $text = "OCT";
                                            break;
                                        case 11: $text = "NOV";
                                            break;
                                        case 12: $text = "DEC";
                                            break;
                                    }
                                    if ($month == 1) {
                                        $month = 12;
                                        $year--;
                                    } else
                                        $month--;
                                    echo "<th>" . $text . "-" . $year . "</th>";
                                    $count++;
                                }
                                ?>
                            </tr>
                            <?php
                            include './functions/DBConnect.php';
                            $sql = "select * from member_detail";
                            $result = mysqli_query($con, $sql);
                            while ($row = mysqli_fetch_assoc($result)) {

                                echo '<tr>';
                                echo "<td>" . $row['fname'] . " " . $row['lname'] . " </td>";
//                    echo '<td><a href="monthlyAttendanceAction.php">Do</a></td>';
                                $month = date("m");
                                $month = intval($month);
                                $count = 0;
                                $year = date("Y");

                                $miiiid = $row['memb_id'];
                                $sql2 = "select doj from member_detail where memb_id='$miiiid'";
                                $resultx = mysqli_query($con, $sql2);
                                $rowx = mysqli_fetch_assoc($resultx);
                                $doj = substr($rowx['doj'], 0, 8);

                                $sqlx = "select sum(amount) as total from payment where type='admission' and mem_id='$miiiid'";
                                $resultp = mysqli_query($con, $sqlx);


                                $rowpx = mysqli_fetch_assoc($resultp);
                                if ($rowpx['total'] != NULL)
                                    $paidAdmission = $rowpx['total'];
                                else
                                    $paidAdmission = 0;
                                echo '<td> Rs.' . $paidAdmission . '</td>';

                                $mnt = date('m');
                                $yr = date('Y');
                                $sqlnow = "select sum(amount) as total from monthly where mem_id='$miiiid' and (year,month) > ($yr,$mnt)";
                                $resultnow = mysqli_query($con, $sqlnow);
                                $rownow = mysqli_fetch_assoc($resultnow);
                                if ($rownow['total'] == NULL)
                                    $advan = 0;
                                else
                                    $advan = $rownow['total'];
                                echo '<td> Rs.' . $advan . '</td>';


                                while ($count < 5) {
                                    $sql = "select * from members_in_month where memb_id='" . $miiiid . "' and month='" . $month . "' and year='" . $year . "'";
//                        echo $sql."*-*-*-*<br/>";
                                    $internal_result = mysqli_query($con, $sql);
                                    if (mysqli_num_rows($internal_result) == 0) {

                                        if ($month < 10)
                                            $mmy = $year . "-0" . $month;
                                        else
                                            $mmy = $year . "-" . $month;
                                        if (strcmp($mmy, $doj) >= 0)
                                            echo '<td>  <span>Set <a href="monthlyAttendance.php">attendance</a> first</span></td>';
                                        else
                                            echo '<td > 
                                        <span    style="color:grey;"class="glyphicon glyphicon-ban-circle"></span>
                                        NA 
                                     </td>';
                                    }
                                    else {
                                        $internal_row = mysqli_fetch_assoc($internal_result);
                                        if ($internal_row['status']) {
                                            $sqlnow = "select sum(amount) as total from monthly where mem_id='" . $miiiid . "' and month='" . $month . "' and year='" . $year . "'";
//                                echo $sqlnow;
                                            $resultnow = mysqli_query($con, $sqlnow);
                                            $rownow = mysqli_fetch_assoc($resultnow);
                                            if ($rownow['total'] == NULL)
                                                $paid = 0;
                                            else
                                                $paid = $rownow['total'];
                                            echo '<td>Rs.' . $paid . '</td>';
                                        } else
                                            echo '<td><span class="glyphicon glyphicon-remove-sign" style="color: red"></span>ABSENT</td>';
                                    }
                                    if ($month == 1) {
                                        $month = 12;
                                        $year--;
                                    } else
                                        $month--;
                                    $count++;
                                }
                                echo '</tr>';
                            }
                            ?>
                        </table>
                    </div>
                </div>




            </div>
            <div class="row" style="padding-bottom: 8%;">
                <div class="col-lg-3"></div>
                <div class="col-lg-6" id="lasttext"><span>Lorem Ipsum is a dummy text that is mainly used by the printing and design industry Lorem Ipsum is a dummy text that is mainly used by the printing and design industry.</span></div>
                <div class="col-lg-3"></div>
            </div>

            <div class="row" id="footer">
                <center><p>footer section</p></center>
            </div>
        </div>
    </body>