<?php
session_start();
if (!isset($_SESSION["user"])) {
    $_SESSION["message"] = "Unauthorised Access";
    header("Location: login.php");
    
}
$details_member="";
if(isset($_SESSION["details_member"])){
    $details_member=$_SESSION["details_member"];
}
?>
<?php
//$mem_id=""
include './functions/DBConnect.php';
include './functions/datepicker_jquery.php';
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Untitled Document</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="homestyle.css">
        <style>
            #menu button
            {
                height: 60px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" id="head">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6" id="logo">
                    <center><img src="images for html/ALLEPPEY GYM white.png">
                        <h2><b>ALLEPPY GYM</b></h2>
                        <p>Welcome admin</p></center>
                </div>
                <div class="col-lg-3" id="logohome">
                    <p1>home &nbsp; |&nbsp; logout</p1>
                </div>
            </div>	
            <div class="contentwrapper">
                <div class="row" > 

                    <nav>
                        <span class="menu-btn">MENU</span>
                        <ul class="menu">
                            <li><a href="dashboard.php"><button type="button" class="w3-btn">Dashoard</button></a></li>
                            <li><a href="memberdetails.php">
                                    <button type="button" class="w3-btn">Member details</button></a></li>
                            <li><a href="feeDetails.php"> <button type="button" class="w3-btn">Fees details</button></a></li>
                            <li><a href="addmember.php">
                                    <button type="button" class="w3-btn">Add new member</button></a></li>

                            <li><a href="monthlyAttendance.php">
                                    <button type="button" class="w3-btn">Monthly Attendance </button></a></li>
                            <li><a href="addPayment.php">
                                    <button type="button" class="w3-btn">Add Payment</button></a></li>
                            <li><a href="#">
                                    <button type="button" class="w3-btn">Change Password</button></a></li>
                        </ul>

                    </nav> 
                </div>
            </div>
            <div class="row" id="search">
                <div class="col-lg-3"> </div>
                <div class="col-lg-6">
                    <div class="row">
                        <center>
                            <form action="memberdetails.php" method="post">
                                <div class="input-group">
                                    <input list="name_list" autocomplete="off" name="member" autofocus required  class="form-control" placeholder="Search...."/>
                                    <datalist id="name_list">

                                        <?php
                                        $sql = "select * from member_detail";
                                        $result = mysqli_query($con, $sql);
                                        while ($row = mysqli_fetch_assoc($result)) {
                                            echo "<option>" . $row['fname'] . " " . $row['lname'] . " - " . $row['dob'] . "</option>";
                                        }
                                        ?>                
                                    </datalist>                              
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit" name="searchUser"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </center>
                    </div><br>
                    <div class="row">
<?php
if (isset($_POST['searchUser']) || $details_member!="") {
    if(isset($_POST['searchUser']))
    {
        $string = $_POST["member"];
        $len = strlen($string);
        $arr = explode(' ', trim($string));
        $first_name = $arr[0];
        $date_of_birth = substr($string, ($len - 10), 10);      
        $sql = "select * from member_detail where fname='" . $first_name . "'and dob='" . $date_of_birth . "'";
    }
    else
        $sql="select * from member_detail where memb_id='$details_member'";
    
    $result = mysqli_query($con, $sql);
    if ($row = mysqli_fetch_assoc($result)) {
        $mid = $row['memb_id'];
        ?>

                                <div class="col-lg-7" id="search-content">
                                    <table >
        <?php
        echo '<tr><td colspan="2">&nbsp;&nbsp;Id No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>: ' . $mid . '</td></tr>';
        echo '<tr><td colspan="2">&nbsp;&nbsp;Name </td><td>: ' . $row['fname'] . ' ' . $row['lname'] . '</td></tr>';
        echo '<tr><td colspan="2">&nbsp;&nbsp;Date of Birth </td><td>: ' . $row['dob'] . '</td></tr>';
        echo '<tr><td colspan="2">&nbsp;&nbsp;Date of Join </td><td>: ' . $row['doj'] . '</td></tr>';
        echo '<tr><td colspan="2">&nbsp;&nbsp;Blood Group </td><td>: ' . $row['bl_grp'] . '</td></tr>';
        echo '<tr><td colspan="2">&nbsp;&nbsp;Mobile </td><td> : ' . $row['mobile'] . '</td></tr>';
        echo '<tr><td colspan="2">&nbsp;&nbsp;E-mail </td><td> : ' . $row['mail'] . '</td></tr>';
        echo '<tr><td colspan="2">&nbsp;&nbsp;Address </td><td rowspan="4">: ' . $row['hname'] . ', ' . $row['street'] . ',<br/>' . $row['city'] . ', PIN: ' . $row['pin'];

        $sql = "select * from physique where mem_id='" . $mid . "' order by date desc";
        $result = mysqli_query($con, $sql);
        ?>
                                    </table>
                                </div>
                                <div class="col-lg-4" id="search-img"><br><br>
                                    <img src="profile_picture/<?php echo $mid; ?>.jpg" class="img-responsive" alt="image" width="100" height="100">  

                                </div>
                            </div>
                    
                    
                            <div class="row" style="overflow-x:auto; margin-top:2%;" id="table-box">
                                <center><h4>FITNESS DETAILS</h4></center>
                                <center><table class="table table-bordered" id="tb-box">
                                        <thead>
                                            <tr>
                                                <th>DATE</th>
                                                <th>HEIGHT</th>
                                                <th>WAIST</th>
                                                <th>WEIGHT</th>
                                                <th>CHEST</th>
                                                <th>ARMS</th>
                                                <th>THIGHS</th>
                                                <th>CALF</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                        <form action="memberdetailsAction.php" method="post"> 
                                            <td style="width: 50px;"><input type="text" style="width: 100%;"name="dom" id="datepicker"/></td>
                                            <td style="width: 50px;"><input type="text" style="width: 100%;" name="height"/></td>
                                            <td style="width: 50px;"><input type="text" style="width: 100%;" name="weight"/></td>
                                            <td style="width: 50px;"><input type="text" style="width: 100%;" name="waist"/></td>
                                            <td style="width: 50px;"><input type="text" style="width: 100%;" name="chest"/></td>
                                            <td style="width: 50px;"><input type="text" style="width: 100%;" name="arms"/></td>
                                            <td style="width: 50px;"><input type="text" style="width: 100%;" name="thighs"/></td>
                                            <td style="width: 50px;">
                                                <input type="text" style="width:100%" name="calf"/>
                                                <input type="hidden" value="<?php echo $mid ?>" name="member_id"/>
                                            </td>
                                            <td style="width: 40px;">
                                                <input type="submit" style="width: 100%;" name="addPhysique" value="Add Data"/>
                                            </td>
                                        </form>
                                        </tr>
        <?php
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<tr>';
            echo '<td>' . $row['date'] . '</td>';
            echo '<td>' . $row['height'] . '</td>';
            echo '<td>' . $row['weight'] . '</td>';
            echo '<td>' . $row['waist'] . '</td>';
            echo '<td>' . $row['chest'] . '</td>';
            echo '<td>' . $row['arms'] . '</td>';
            echo '<td>' . $row['thighs'] . '</td>';
            echo '<td>' . $row['calf'] . '</td>';
            echo '</tr>';
        }
        ?>

                                        </tbody>
                                    </table></center>
                                        <?php
                                    } else {
                                        echo '<div class="row"><div class="col-md-4"></div>';
                                        echo '<div class="col-md-4">NO DATA SELECTED, Please Select Click data from suggestions only to search !</div>';
                                        echo '<div class="col-md-4"></div></div>';
                                    }
                                }
                                ?>

                    </div></div>


            </div>
            <div class="row" id="footer">
                <center><p>footer section</p></center>
            </div>
        </div>
    </div>


</body>
</html>