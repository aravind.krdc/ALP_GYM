function letterOnly(value, callerElement, msgElement)
{
    if (/[0-9!@#$%^&*()+=_.,<>?:;"'{}-]/.test(value))
    {
        document.getElementById(callerElement).value = "";
        document.getElementById(msgElement).style.display = 'block';
    } else
        document.getElementById(msgElement).style.display = 'none';
}

function numberOnly(value, callerElement, msgElement)
{
    if (/\D/.test(value))
    {
        document.getElementById(callerElement).value = "";
        document.getElementById(msgElement).style.display = 'block';
    } else
        document.getElementById(msgElement).style.display = 'none';
}
function mailId(value, callerElement, msgElement)
{
    var atpos = value.indexOf("@");
    var dotpos = value.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        document.getElementById(callerElement).value = "";
        document.getElementById(msgElement).style.display = 'block';
    } else
        document.getElementById(msgElement).style.display = 'none';
}

function isDate(value, callerElement, msgElement)
{
    if (isNaN(value.getTime()))
    {
        document.getElementById(callerElement).value = "";
        document.getElementById(msgElement).style.display = 'block';
    } else
        document.getElementById(msgElement).style.display = 'none';
}

function jpgExtension(callerElement, msgElement)
{
    var a="#"+callerElement;
    var file = document.querySelector(a);
    if (/\.(jpg)$/i.test(file.files[0].name) === false) 
    {
        alert("not an image!");
    }
}