<!DOCTYPE html>
<html>
<head>
 <!-- Loading jQuery UI Style  -->
 <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
 <!-- Loading jQuery Library JS -->
 <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
 <!-- Loading jQuery UI JS -->
 <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

 <script type="text/javascript">
  $(document).ready(function(){
   $('#datepicker').datepicker({
    changeMonth: true,// This option allow user to change month on top bar
    changeYear:true,// This option allow user to change year on top bar of date picker
    yearRange: "1915:+0",
    dateFormat: 'yy-mm-dd' 
    
   });
  });
  $(document).ready(function(){
   $('#datepicker2').datepicker({
    changeMonth: true,// This option allow user to change month on top bar
    changeYear:true,// This option allow user to change year on top bar of date picker
    yearRange: "1915:+0",
    dateFormat: 'yy-mm-dd' 
    
   });
  });
 </script>
</head>

</html> 