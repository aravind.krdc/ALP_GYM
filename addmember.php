 <?php
   session_start();
   if (!isset($_SESSION["user"]))
   {
       $_SESSION["message"]="Unauthorised Access";
       header("Location: login.php");
   }    
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="homestyle.css">
  
  <style>
      #menu button
            {
                height: 60px;
            }
  </style>
</head>
 <?php
//   session_start();
   $message = "";
   if (isset($_SESSION["msg"]))
    $message = $_SESSION["msg"];
   
   
    include './functions/datepicker_jquery.php';
    
?>
<body>
	<div class="container-fluid">
    <div class="row" id="head">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6" id="logo">
            <center><img src="images for html/ALLEPPEY GYM white.png">
            <h2><b>ALLEPPY GYM</b></h2>
            <p>Welcome admin</p></center>
        </div>
        <div class="col-lg-3" id="logohome">
            <p1>home &nbsp; |&nbsp; logout</p1>
        </div>
 </div>
 	<div class="contentwrapper">
                <div class="row" > 

                    <nav>
                    <span class="menu-btn">MENU</span>
                        <ul class="menu">
                            <li><a href="dashboard.php"><button type="button" class="w3-btn">Dashoard</button></a></li>
                            <li><a href="memberdetails.php">
                                <button type="button" class="w3-btn">Member details</button></a></li>
                            <li><a href="feeDetails.php"> <button type="button" class="w3-btn">Fees details</button></a></li>
                            <li><a href="addmember.php">
                                <button type="button" class="w3-btn">Add new member</button></a></li>
                           
                            <li><a href="monthlyAttendance.php">
                                <button type="button" class="w3-btn">Monthly Attendance </button></a></li>
                             <li><a href="addPayment.php">
                                <button type="button" class="w3-btn">Add Payment</button></a></li>
                              <li><a href="#">
                                <button type="button" class="w3-btn">Change Password</button></a></li>
                        </ul>

                    </nav>                 
        

                   <!-- <center> 
                        <div  class="col-lg-2">
                            <a href="dashboard.php">
                                <button type="button" class="w3-btn">Dashoard</button>
                            </a>
                        </div>
                        <div  class="col-lg-2">
                            <a href="memberdetails.php">
                                <button type="button" class="w3-btn">Member details</button>
                            </a>
                        </div>
                        <div  class="col-lg-2">
                            <a href="" >
                                <button type="button" class="w3-btn">Fees details</button>
                            </a>
                        </div>
                        <div  class="col-lg-2">
                            <a href="addmember.php">
                                <button type="button" class="w3-btn">Add new member</button>
                            </a>
                        </div>
                        
                        <div  class="col-lg-2">
                            <a href="monthlyAttendance.php">
                                <button type="button" class="w3-btn">Monthly Attendance </button>
                            </a>
                        </div>
                        
                        <div  class="col-lg-2">
                            <a href="addPayment.php">
                                <button type="button" class="w3-btn">Add Payment</button>
                            </a>
                        </div>
                    </center>-->
                    </div>
                </div>
    	<div class="row">
    		<div class="col-lg-3">
    		</div>
	    		<div class="col-lg-6" id="form-bg">
	    			<form action="addMemberAction.php" method="post" enctype="multipart/form-data" class="form-horizontal">
	    				
	    					<div class="col-lg-5" >
						    <label for="name" class="common">FIRST NAME</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="fname">
						    </div>
	    				<div class="clearfix"></div><br>
	    				
	    					<div class="col-lg-5 " >
						    <label for="name" class="common">LAST NAME</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="lname">
						    </div>
	    				<div class="clearfix"></div><br>
	    				
	    					<div class="col-lg-5 " >
						    <label for="gend" class="common">GENDER</label>
						    </div>
						    <div class="col-lg-7">
						    <input type="radio" name="gender" value="m"/> Male
						    <input type="radio" name="gender" value="f"/> Female
						    </div>
	    				<div class="clearfix"></div><br>
	    				
	    					<div class="col-lg-5 " >
						    <label for="dob" class="common">DATE OF BIRTH</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" id="datepicker" name="dob">
						    </div>
	    				<div class="clearfix"></div><br>
	    				
	    					<div class="col-lg-5 " >
						    <label for="addr" class="common">HOUSE NAME</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="hname">
						    </div>
	    				<div class="clearfix"></div><br>
	    				
	    					<div class="col-lg-5 " >
						    <label for="addr" class="common">STREET</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="street">
						    </div>
	    				<div class="clearfix"></div><br>
	    				
	    					<div class="col-lg-5 " >
						    <label for="addr" class="common">CITY</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="city">
						    </div>
	    				<div class="clearfix"></div><br>
	    				
	    					<div class="col-lg-5 " >
						    <label for="addr" class="common">PIN</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="pin">
						    </div>
	    				<div class="clearfix"></div><br>
	    				
	    					<div class="col-lg-5 " >
						    <label for="cont" class="common">MOBILE</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="mobile">
						    </div>
	    				<div class="clearfix"></div><br>
	    				
	    					<div class="col-lg-5 " >
						    <label for="cont" class="common">LAND PHONE</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="land">
						    </div>
	    				<div class="clearfix"></div><br>
	    					<div class="col-lg-5 " >
						    <label for="cont" class="common">e MAIL ID</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="mail">
						    </div>
	    				<div class="clearfix"></div><br>
	    					<div class="col-lg-5 " >
						    <label for="doj" class="common">DATE OF JOINING</label>
						    </div>
						    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="doj" id="datepicker2">
						    </div>
	    				<div class="clearfix"></div><br>
	    					<div class="col-lg-5 " >
						    <label for="bld" class="common">BLOOD GROUP</label>
						    </div>
						    <div class="col-lg-7">
                                                    <select name="bl_grp" class="form-control">
                                                        <option value="Not Specified">-- Blood Group --</option>
                                                        <option value="A +ve">A +ve</option>
                                                        <option value="A -ve">A -ve</option>
                                                        <option value="B +ve">B +ve</option>
                                                        <option value="B -ve">B -ve</option>
                                                        <option value="O +ve">O +ve</option>
                                                        <option value="O -ve">O -ve</option>
                                                        <option value="AB +ve">AB +ve</option>
                                                        <option value="AB -ve">AB -ve</option>
                                                    </select>
						    </div>
	    				<div class="clearfix"></div><br>
	    					<div class="col-lg-5 " >
						    <label for="img" class="common">ADD IMAGE</label>
						    </div>
						    <div class="col-lg-7">
						    <input type="file" id="image" class="form-control file" name="imageFile" accept=".jpg"/><br>
						    </div>
	    				<br><br>
	    				<center><button type="submit" class="btn btn-danger">ADD MEMBER</button></center>
	    				
	    				
	    				

	    				
	    			</form>

	    		</div>

    		<div class="col-lg-3">
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-lg-3"></div>
    		<div class="col-lg-6" id="lasttext"><span>Lorem Ipsum is a dummy text that is mainly used by the printing and design industry Lorem Ipsum is a dummy text that is mainly used by the printing and design industry.</span></div>
    		<div class="col-lg-3"></div>
    	</div>

    	<div class="row" id="footer">
        	<center><p>footer section</p></center>
    	</div>
    </div>
</body>
</html>