<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>jQuery UI Effects - Show Demo</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            function runEffect() {
                        $("#effect").show();
                    }
        </script>
    </head>
    <body>

        <div class="toggler">
            <div id="effect" style="display: none"class="ui-widget-content ui-corner-all">
                <h3 class="ui-widget-header ui-corner-all">Show</h3>
                <p>
                    Etiam libero neque, luctus a, eleifend nec, semper at, lorem. Sed pede. Nulla lorem metus, adipiscing ut, luctus sed, hendrerit vitae, mi.
                </p>
            </div>
        </div>

        <select name="effects" id="effectTypes">
            <option value="bounce">Bounce</option>  
        </select>

        <button id="button" class="ui-state-default ui-corner-all" onclick="runEffect()">Run Effect</button>


    </body>
</html>