<!doctype html>
<?php
session_start();
if (!isset($_SESSION["user"])) {
    $_SESSION["message"] = "Unauthorised Access";
    header("Location: login.php");
}
?>


<html>
    <head>
        <meta charset="utf-8">
        <title>DASHBOARD </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="homestyle.css">
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <!--<div id="container"></div>-->

        <style>
            #menu button
            {
                height: 60px;
            }
            #graph
            {
                max-height: 260px;
                height: 220px;
            }
            #currentmember
            {
                height: 255px;
            }

            #newmembers
            {
                height: 255px;
            }
        </style>


    </head>

    <body>
        <div class="container-fluid">
            <div class="row" id="head">
                <div class="col-md-3">
                </div>
                <div class="col-md-6" id="logo">
                    <center><img src="images for html/ALLEPPEY GYM white.png">
                        <h2><b>ALLEPPY GYM</b></h2>
                        <p>Welcome admin</p></center>
                </div>
                <div class="col-md-3" id="logohome">
                    <p>home &nbsp; |&nbsp; logout</p>
                </div>
            </div>

            <div class="contentwrapper">
                <div class="row" > 

                    <nav>
                        <span class="menu-btn">MENU</span>
                        <ul class="menu">
                            <li><a href="dashboard.php"><button type="button" class="w3-btn">Dashoard</button></a></li>
                            <li><a href="memberdetails.php">
                                    <button type="button" class="w3-btn">Member details</button></a></li>
                            <li><a href="feeDetails.php"> <button type="button" class="w3-btn">Fees details</button></a></li>
                            <li><a href="addmember.php">
                                    <button type="button" class="w3-btn">Add new member</button></a></li>

                            <li><a href="monthlyAttendance.php">
                                    <button type="button" class="w3-btn">Monthly Attendance </button></a></li>
                            <li><a href="addPayment.php">
                                    <button type="button" class="w3-btn">Add Payment</button></a></li>
                            <li><a href="#">
                                    <button type="button" class="w3-btn">Change Password</button></a></li>
                        </ul>

                    </nav>         

                </div></div>
            <div class="clearfix"></div>

            <div class="container">
                <div class="row" id="middle">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-8" id="middlecontent">
                        <div class="row">
                            <div class="col-md-6" id="middlebox1">
                                <div class="row" id="feesummary">
                                    <?php
                                    include './functions/DBConnect.php';

                                    $sql1 = "select * from members_in_month where status=1 and 
                                            (month,year,memb_id) not in 
                                                (select month,year,mem_id from monthly group by month having sum(amount)=300)";
                                    $result = mysqli_query($con, $sql1);
                                    $num = mysqli_num_rows($result);
                                    ?>



                                    <center><p>FEE SUMMARY</p></center>

                                </div>

                                <div class="row" id="pendingfees">

                                    <center><div class="col-md-8 col-md-push-2" id="pending">
                                            <p>Pending fees&nbsp;&nbsp;&nbsp;<?php echo $num ?></p>
                                        </div>
                                    </center>
                                    <?php
                                    $m = date("m");
                                    $y = date("Y");
                                    $sql2 = "select * from monthly where year>$y and month>$m";
                                    $resultp = mysqli_query($con, $sql2);
                                    $adv = mysqli_num_rows($resultp);
                                    ?>
                                </div>
                                <div class="row" id="pendingfees">
                                    <center><div class="col-md-8 col-md-push-2" id="payment">
                                            <p>Advance payment&nbsp;&nbsp;&nbsp;<?php echo $adv ?></p>
                                        </div>
                                    </center>
                                </div>
<?php
$sql2 = "select sum(amount) as total from payment where date like '" . $y . "-" . $m . "%'";
//    echo $sql2;
$resultp = mysqli_query($con, $sql2);
$rowp = mysqli_fetch_assoc($resultp);
$sum = $rowp['total'];
if ($sum == NULL)
    $sum = 0;
?>
                                <div class="row" id="income">
                                    <center><div class="col-md-8 col-md-push-2" id="payment">
                                            <p>Income this month&nbsp;&nbsp;&nbsp;Rs. <?php echo $sum ?></p>
                                        </div>
                                    </center>
                                </div>
                            </div>
                            <div class="col-md-6" id="middlebox2">
                                <div class="row" id="feesummary">

                                    <center><p>FEE SUMMARY</p></center>
                                </div>
                                <div class="row" id="graph">



                                </div>
                                <!--	    					<div class="row" id="member">
                                                                                    <p>HAHAHAH</p>
                                                                                </div>-->
                            </div>
                        </div>
                        <!--=================================middlesecond======================================-->
                        <div class="row" id="middlesecond">
                            <div class="col-md-6" id="middlebox3">
                                <div class="row">



<?php
$m = date("m");
$y = date("Y");
$sql = "select * from members_in_month where month=$m and year= $y";
$result = mysqli_query($con, $sql);
$member_count = mysqli_num_rows($result);
if ($m == 1) {
    $m = 12;
    $y = $y - 1;
} else {
    $m = $m - 1;
}
$sql = "select * from members_in_month where month= $m and year= $y";
$result = mysqli_query($con, $sql);
$member_count_prev = mysqli_num_rows($result);

$diff = $member_count - $member_count_prev;


$sql = "select distinct memb_id from members_in_month where year= $y";
$result = mysqli_query($con, $sql);
$member_year = mysqli_num_rows($result);

$sql = "select distinct memb_id from members_in_month where year= $y-1";
$result = mysqli_query($con, $sql);
$member_Lyear = mysqli_num_rows($result);
$diffY = $member_year - $member_Lyear;
?>
                                    <div class="col-md-6" id="currentmember">


                                        <div class="row" id="currentactive">
                                            <center><span>CURRENT MEMBERS</span></center>
                                            <center><div class="col-md-10" id="active">
                                                    <p><?php echo $member_count ?> active members</p>
                                                </div></center>
                                        </div>
                                        <div class="row" id="middlelast">
<?php
if ($diff >= 0)
    echo '<center style="background-color: green; padding-bottom: 16%;"><p> +' . $diff . '</p><span>Based on Previous Month</span></center>';
else
    echo '<center style="background-color: red; padding-bottom: 16%;"><p> -' . $diff . '</p>Based on Previous Month</center>'
    ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6" id="newmembers">

                                        <div class="row" id="redtop">
                                            <?php
                                            if ($diff >= 0)
                                                echo '<center style="background-color: green;padding-bottom: 14%;"><p> +' . $diffY . '</p><span>This Year</span><br/></center>';
                                            else
                                                echo '<center style="background-color: green;padding-bottom: 14%;"><p> -' . $diffY . '</p><span>This Year</span><br/></center>';
                                            ?>
                                        </div>
                                        <div class="row" id="new">
                                            <center><p style="font-size: 25px;">
                                            <?php echo $member_year; ?>
                                                </p><p>MEMBERS THIS YEAR</p></center>
                                        </div>


                                    </div>
                                </div>

                            </div>	


                            <div class="col-md-6" id="middlebox2">
                                <div class="row" id="box4">
                                    <div class="col-md-7 exp" >
                                        <center><div class="row" id="expiringmember">
                                                <p> EXPIRING MEMBERS</p>
                                            </div></center>
                                    </div>
                                    <div class="col-md-5 exp1" >
                                        <div class="row" id="box4left">
                                            <p>0</p>
                                        </div>
                                    </div>

                                </div>
                            </div>



                        </div>
                        <div class="row" id="lasttext">
                            <span>Lorem Ipsum is a dummy text that is mainly used by the printing and design industry Lorem Ipsum is a dummy text that is mainly used by the printing and design industry.</span>
                        </div>

                        <div class="col-md-2">
                        </div>	
                    </div>
                </div>



            </div>

            <div class="row" id="footer">
                <center><p>footer section</p></center>
            </div>

        </div>
    </body>
    <script>
        Highcharts.chart('graph', {
            title: {
                text: ' '
            },            
            yAxis: {
                title: {
                    text: 'Members'
                }
            },
            xAxis: {
                title: {
                    text: 'Months'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    pointStart: 3
                }
            },
<?php
$a = 0;
$month = date("m");
$year = date("Y");

while ($a < 5) {
    $result = mysqli_query($con, "select * from members_in_month where month= $month and year= $year and status=1");
//        echo "select * from members_in_month where month= $month and year= $year";
    $count[$a] = mysqli_num_rows($result);
//        echo 'COUNT: '.$count[$a];
    if ($month == 1) {
        $month = 12;
        $year--;
    } else
        $month--;
    $a++;
}
?>
            series: [{name: 'MEMBERS',
                    data: [<?php echo $count[4] . "," . $count[3] . "," . $count[2] . "," . $count[1] . "," . $count[0] ?>]}]

        });
    </script>
<?php // echo $count[1].",".$count[2].",".$count[3].",".$count[4].",".$count[5] ?> ]

    <script>
        $('span.menu-btn').click(function () {
            $('ul.menu').toggle();
        })
        $('window').resize(function () {
            if ($('window').width() > 600) {
                $('ul.menu').removeAttr('style');
            }
        })
    </script>
</html>
