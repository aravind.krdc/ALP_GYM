 <?php
   session_start();
   if (!isset($_SESSION["user"]))
   {
       $_SESSION["message"]="Unauthorised Access";
       header("Location: logoutAction.php");
   }    
?>

 <?php
include './functions/DBConnect.php';

if (isset($_POST['addAmount'])) {
    $mem_id = $_POST['member'];
    $advance = $_POST['advance'];
    $count = $_POST['opt_count'];
    $count_chkd = 0;
    $month_string = array();
    while ($count > 0) {
        $i = 'm' . $count;
        if (isset($_POST[$i]))
            $month_string[$count_chkd] = $_POST[$i];
        else {
            $count--;
            continue;
        }
        $count--;
        $count_chkd++;
    }
    $amount_recvd   =   0;
    if (isset($_POST['amount']))
    {
        $amount_recvd = $_POST['amount'];
        $amount = $_POST['amount'] + $advance;
    }
    else
        $amount = $advance;
    $sql = "select * from fee_structure order by fee_id desc";
    $result = mysqli_query($con, $sql);
    $row = mysqli_fetch_assoc($result);
    $admission = $row['admission'];
    $monthly = $row['monthly'];

    global $admission;
    global $monthly;

    $sql = "select sum(amount) as total_paid_adm from payment where mem_id='$mem_id' and type='admission'";
    $result = mysqli_query($con, $sql);
    $row = mysqli_fetch_assoc($result);
    $total_paid_adm = $row['total_paid_adm'];
    $remain_adm = $admission - $total_paid_adm;
    payment($mem_id, $amount, $amount_recvd, $remain_adm, $month_string,$advance);
}

function payment($mem_id, $amount, $amount_recvd, $remain_adm, $month_string,$advance) {
    global $con;
    $paid=0;
    if ($amount <= $remain_adm) {
        $id = "PAY" . row_counter("select * from payment");
        $sql = "insert into payment ('$id','$mem_id',curdate(),$amount,'admission')";
        $paid=$amount;
        $amount = 0;
        mysqli_query($con, $sql);        
    } else if ($remain_adm > 0) {
        $id = "PAY" . row_counter("select * from payment");
        $sql = "insert into payment values ('$id','$mem_id',curdate(),$remain_adm,'admission')";
        $paid=$remain_adm;
        mysqli_query($con, $sql);
        $amount -= $remain_adm;
    }
    if ($amount > 0) {
        $pay_id = "PAY" . row_counter("select * from payment");
        if($advance>=$paid)
            $atx    =   $amount_recvd;
        else
            $atx    =   $amount-$paid;
        $sql = "insert into payment values ('$pay_id','$mem_id',curdate(),$atx,'monthly')";
        mysqli_query($con, $sql);
        if(count($month_string)>0)
            payMonth($mem_id, $month_string, $amount, $pay_id);
        else
        {
            $sql    =   "update advance set amount= $atx where mem_id='$mem_id'";
            mysqli_query($con, $sql);
        }
    }
}

function payMonth($mem_id, $month_string, $amount, $pay_id) {
    global $monthly;
    global $con;
    $arr_size = count($month_string);
    $index = 0;
    while ($amount > $monthly) {
        if($arr_size > 0)
        {
            $id = "MON" . row_counter("select * from monthly");
            $subs = explode('-', $month_string[$index]);
            $mm = getNumber($subs[0]);
            $yy = $subs[1];
            $am = $subs[2];
            $sql = "insert into monthly values ('$id',$mm,$yy,$am,'$mem_id','$pay_id')";
            mysqli_query($con, $sql);
            $amount -= $am;
            $arr_size--;
            $index++;
        }
        else
            break;
    }
    if ($arr_size > 0 && $amount>0) {
        if($amount<$monthly)
        {
            $id = "MON" . row_counter("select * from monthly");
            $subs = explode('-', $month_string[$index]);
            $mm = getNumber($subs[0]);
            $yy = $subs[1];
            $am = $subs[2];
            $sql = "insert into monthly values ('$id',$mm,$yy,$amount,'$mem_id','$pay_id')";
            mysqli_query($con, $sql);
            $amount = 0;
        }
    }
    
    $sql = "update advance set amount=$amount where mem_id='$mem_id'";
    mysqli_query($con, $sql);
}

function getNumber($mon) {
    $monthNum = "";
    switch ($mon) {
        case "JAN": $monthNum = 1;
            break;
        case "FEB": $monthNum = 2;
            break;
        case "MAR": $monthNum = 3;
            break;
        case "APR": $monthNum = 4;
            break;
        case "MAY": $monthNum = 5;
            break;
        case "JUN": $monthNum = 6;
            break;
        case "JUL": $monthNum = 7;
            break;
        case "AUG": $monthNum = 8;
            break;
        case "SEP": $monthNum = 9;
            break;
        case "OCT": $monthNum = 10;
            break;
        case "NOV": $monthNum = 11;
            break;
        case "DEC": $monthNum = 12;
            break;
    }
    return $monthNum;
}
?>






<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="homestyle.css">

        <title>Add Pay Dtls: UNCLR</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        
        <style>         
            #checkboxes {
                display: none;
                border: 1px #dadada solid;
                padding: 5px;               
            }

            #checkboxes label {
                display: block;
            }
            #checkboxes label:hover {
                background-color: #1e90ff;
            }

        </style>
        <script>
            function getData(str) {
                if (str == "") {
                    alert('no data');
                } else {
                    $.post("jquery_advance.php", {name: str}, function (data) {
                        document.getElementById('advance').value = data;
                    });
                    $.post("jquery_pending.php", {memb: str}, function (data) {
                        document.getElementById('checkboxes').innerHTML = data;
                        document.getElementById('checkboxes').style.display="block";
                    });
                }
            }

        </script>

        <style>
            #menu button
            {
                height: 60px;
            }
        </style>
    </head>
    <?php
//    session_start();
//    $message = "";
//    if (isset($_SESSION["msg"]))
//        $message = $_SESSION["msg"];


//    include './functions/datepicker_jquery.php';
    ?>
    <body>
        <div class="container-fluid">
            <div class="row" id="head">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6" id="logo">
                    <center><img src="images for html/ALLEPPEY GYM white.png">
                        <h2><b>ALLEPPY GYM</b></h2>
                        <p>Welcome admin</p></center>
                </div>
                <div class="col-lg-3" id="logohome">
                    <p1><a href="logoutAction.php">logout</a></p1>
                </div>
            </div>
            <div class="contentwrapper">
                <div class="row" > 

                    <nav>
                    <span class="menu-btn">MENU</span>
                        <ul class="menu">
                            <li>
                                <a href="dashboard.php">
                                    <button type="button" class="w3-btn">Dashoard</button>
                                </a>
                            </li>
                            <li>
                                <a href="memberdetails.php">
                                    <button type="button" class="w3-btn">Member details</button>
                                </a>
                            </li>
                            <li>
                                <a href="feeDetails.php"> 
                                    <button type="button" class="w3-btn">Fees details</button>
                                </a>
                            </li>
                            <li>
                                <a href="addmember.php">
                                    <button type="button" class="w3-btn">Add new member</button>
                                </a>
                            </li>

                            <li>
                                <a href="monthlyAttendance.php">
                                    <button type="button" class="w3-btn">Monthly Attendance </button>
                                </a>
                            </li>
                            <li>
                                <a href="addPayment.php">
                                    <button type="button" class="w3-btn">Add Payment</button>
                                </a>
                            </li>
                            <li><a href="changePassword.php">
                                    <button type="button" class="w3-btn">Change Password</button>
                                </a>
                            </li>
                        </ul>

                    </nav>                 
        
                    </div>
                </div>

                <div class="row">

                    <div class="row" id="mainprt">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" id="boxprt">

                            <div class="row">

                                <div class="col-sm-12" id="loginheader" style="background-color:#333;">PAYMENT</div>
                                
                                <div class="row" id="loginbody">                                    
                                    <form method="post" action="addPayment.php">
                                        <div class="row">
                                            
                                            <div class="col-lg-12"><br/></div>
                                            
                                            <div class="col-lg-1"></div>
                                            <div class="col-lg-10">
                                                <input list="name_list" name="member" autofocus  required onchange="getData(this.value)"
                                                       id="member_name" class="form-control" placeholder="Type Name to search"/>
                                                
                                            </div>
                                            <datalist id="name_list">
                                                <?php
                                                $sql = "select * from member_detail";
                                                $result = mysqli_query($con, $sql);
                                                while ($row = mysqli_fetch_assoc($result)) {
                                                    echo '<option value="' . $row['memb_id'] . '">' . $row['fname'] . ' ' . $row['lname'] . ' - ' . $row['dob'] . '</option>';
                                                }
                                                ?>                
                                            </datalist> 
                                        </div>
                                        <br/>
                                        
                                        <div class="row">
                                            
                                            <div class="col-lg-1"></div>
                                            
                                            <div class="col-lg-10">
                                                <input type="text" name="advance" class="form-control" id="advance" readonly/><br/>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-lg-10">
                                                <div id="checkboxes" style="display: none"></div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-lg-1"><br/></div>
                                            <div class="col-lg-10">
                                                <input type="text" id="amount" name="amount" class=" form-control col-sm-12" placeholder="AMount"/>
                                            </div>
                                        </div>
                                        
                                        
                                        <br/>
                                        
                                        <br/>
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-lg-10">
                                                <center><input type="submit" class="buttonn" name="addAmount" value="Add"/></center>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <br/>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6" id="lasttext"><span>Lorem Ipsum is a dummy text that is mainly used by the printing and design industry Lorem Ipsum is a dummy text that is mainly used by the printing and design industry.</span></div>
                    <div class="col-lg-3"></div>
                </div>

                <div class="row" id="footer">
                    <center><p>footer section</p></center>
                </div>
        </div>
    </body>


        </html>
