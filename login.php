<?php
session_start();
$msg = "";
if (isset($_SESSION['msg']))
    $msg = $_SESSION['msg'];
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Untitled Document</title>
        <link rel="icon" href="images/icon.png"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="setcss.css">
        <link rel="stylesheet" href="homestyle.css">
    </head>

    <body>
        <div class="container-fluid">


            <div class="well well-lg" id="headbar"><p><strong>ALLEPPY GYM</strong></p></div>

            <div class="row" id="subprt">
                <div class="col-sm-12"></div>
            </div>


            <div class="row" id="mainprt">
                <div class="col-sm-4"></div>
                <div class="col-sm-4" id="boxprt">

                    <div class="row" id="adminlogin">
                        <div class="col-sm-12" id="loginheader" style="background-color:#333;">ADMIN LOGIN</div>
                    </div><!--adminlogin-->
                    <div class="row" id="loginbody">

                        <div class="col-sm-12" style="color: red"><?php echo $msg; ?></div>

                        <form action="loginAction.php" method="post">
                            <div class="form-group">
                                <label for="email"></label>

                                <input type="text" class="form-control" name="username" autofocus required autocomplete="off" id="email" placeholder="Username">

                            </div>
                            <div class="form-group">
                                <label for="pwd"></label>
                                <input type="password" class="form-control" name="password" autocomplete="off" required id="pwd" placeholder="password">
                            </div>
                            <div class="checkbox">
                             <!--<label><input type="checkbox">remember me</label>-->
                              <!--<p>forget password click <u>here!</u></p>-->
                            </div>
                            <center><button type="submit" name="submit" id="loginb" class="btn btn-default">LOGIN</button></center>

                        </form>
                    </div><!--loginbody-->
                </div><!--boxprt-->
                <div class="col-sm-4"></div>
            </div><!--row mainprt-->



        </div><!--container-->

    </body>
</html>

<?php
$_SESSION['msg'] = "";
?>