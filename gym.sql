-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.18-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema alpy_gym
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ alpy_gym;
USE alpy_gym;

--
-- Table structure for table `alpy_gym`.`advance`
--

DROP TABLE IF EXISTS `advance`;
CREATE TABLE `advance` (
  `adv_id` int(10) unsigned NOT NULL auto_increment,
  `mem_id` varchar(45) NOT NULL default '',
  `amount` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`adv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alpy_gym`.`advance`
--

/*!40000 ALTER TABLE `advance` DISABLE KEYS */;
/*!40000 ALTER TABLE `advance` ENABLE KEYS */;


--
-- Table structure for table `alpy_gym`.`fee_structure`
--

DROP TABLE IF EXISTS `fee_structure`;
CREATE TABLE `fee_structure` (
  `fee_id` varchar(16) NOT NULL default '',
  `admission` int(10) unsigned NOT NULL default '0' COMMENT 'fee',
  `monthly` int(10) unsigned NOT NULL default '0' COMMENT 'monthly fee',
  `start_month` int(10) unsigned NOT NULL default '0' COMMENT 'wef month',
  `start_year` int(10) unsigned NOT NULL default '0' COMMENT 'wef year',
  PRIMARY KEY  (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alpy_gym`.`fee_structure`
--

/*!40000 ALTER TABLE `fee_structure` DISABLE KEYS */;
INSERT INTO `fee_structure` (`fee_id`,`admission`,`monthly`,`start_month`,`start_year`) VALUES 
 ('1',500,300,1,2010);
/*!40000 ALTER TABLE `fee_structure` ENABLE KEYS */;


--
-- Table structure for table `alpy_gym`.`login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `username` varchar(32) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alpy_gym`.`login`
--

/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` (`username`,`password`) VALUES 
 ('admin','admin');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;


--
-- Table structure for table `alpy_gym`.`member_detail`
--

DROP TABLE IF EXISTS `member_detail`;
CREATE TABLE `member_detail` (
  `memb_id` varchar(12) NOT NULL default '',
  `fname` varchar(45) default NULL,
  `lname` varchar(45) default NULL,
  `hname` varchar(45) default NULL,
  `street` varchar(45) default NULL,
  `city` varchar(45) default NULL,
  `pin` varchar(45) default NULL,
  `mobile` varchar(45) default NULL,
  `land` varchar(45) default NULL,
  `mail` varchar(45) default NULL,
  `dob` date default NULL,
  `doj` date default NULL,
  `bl_grp` varchar(45) default NULL,
  `gender` varchar(8) NOT NULL default '',
  `status` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`memb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alpy_gym`.`member_detail`
--

/*!40000 ALTER TABLE `member_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `member_detail` ENABLE KEYS */;


--
-- Table structure for table `alpy_gym`.`members_in_month`
--

DROP TABLE IF EXISTS `members_in_month`;
CREATE TABLE `members_in_month` (
  `mim_id` int(10) unsigned NOT NULL auto_increment,
  `memb_id` varchar(16) NOT NULL default '0',
  `month` int(10) unsigned NOT NULL default '0',
  `year` int(10) unsigned NOT NULL default '0',
  `status` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`mim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alpy_gym`.`members_in_month`
--

/*!40000 ALTER TABLE `members_in_month` DISABLE KEYS */;
/*!40000 ALTER TABLE `members_in_month` ENABLE KEYS */;


--
-- Table structure for table `alpy_gym`.`monthly`
--

DROP TABLE IF EXISTS `monthly`;
CREATE TABLE `monthly` (
  `mon_id` varchar(16) NOT NULL default '',
  `month` int(10) unsigned NOT NULL default '0',
  `year` int(10) unsigned NOT NULL default '0',
  `amount` int(10) unsigned NOT NULL default '0',
  `mem_id` varchar(16) NOT NULL default '0',
  `pay_id` varchar(16) NOT NULL default '0',
  PRIMARY KEY  (`mon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alpy_gym`.`monthly`
--

/*!40000 ALTER TABLE `monthly` DISABLE KEYS */;
/*!40000 ALTER TABLE `monthly` ENABLE KEYS */;


--
-- Table structure for table `alpy_gym`.`payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `pay_id` varchar(16) NOT NULL default '',
  `mem_id` varchar(16) NOT NULL default '',
  `date` date NOT NULL default '0000-00-00',
  `amount` int(10) unsigned NOT NULL default '0',
  `type` varchar(16) NOT NULL default '',
  PRIMARY KEY  (`pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alpy_gym`.`payment`
--

/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;


--
-- Table structure for table `alpy_gym`.`physique`
--

DROP TABLE IF EXISTS `physique`;
CREATE TABLE `physique` (
  `phy_id` varchar(45) NOT NULL default '',
  `height` varchar(45) NOT NULL default '',
  `weight` varchar(45) NOT NULL default '',
  `waist` varchar(45) NOT NULL default '',
  `chest` varchar(45) NOT NULL default '',
  `arms` varchar(45) NOT NULL default '',
  `thighs` varchar(45) NOT NULL default '',
  `calf` varchar(45) NOT NULL default '',
  `date` date NOT NULL default '0000-00-00',
  `mem_id` varchar(16) NOT NULL default '',
  PRIMARY KEY  (`phy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alpy_gym`.`physique`
--

/*!40000 ALTER TABLE `physique` DISABLE KEYS */;
/*!40000 ALTER TABLE `physique` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
